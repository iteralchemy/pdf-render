/*
 * Report Template by React-PDF, for friend
 */

// Parameters
const infos = report["sample_info"];
const species = report["report_results"];
const bacteria = species["细菌"];
const fungi = species["真菌"];
const dna_virus = species["DNA病毒"];
const rna_virus = species["RNA病毒"];
const parasite = species["寄生虫"];
const mycobacterium = species["分枝杆菌"];
const mycoplasma = species["支衣原体"];
const background_species = report["report_bg_bacterias"];
const gene = report["selected_rg_reports"];
const stats = report["report_stats"];

// presets
const pics = {
  封面: "public/images/封面.png",
  源古纪logo: "public/images/源古纪logo.png",
  呼研所logo: "public/images/呼研所logo.png",
  惠善logo: "public/images/惠善logo.png",
  章节编号: "public/images/章节编号.png",
  医生签名: "public/images/医生签名.png",
  主任签名: "public/images/主任签名.png",
  惠善章: "public/images/惠善章.png",
  源古纪二维码: "public/images/源古纪二维码.png",
  送检流程: "public/images/送检流程.png",
  替换水印: "public/images/替换水印.jpg",
};

const fonts = {
  思源黑体: {
    name: "source_han_sans",
    src: "public/fonts/SourceHanSans.ttf",
  },
  思源黑体加粗: {
    name: "source_han_sans_bold",
    src: "public/fonts/SourceHanSans-Bold.ttf",
  },
  consola: {
    name: "consola",
    src: "public/fonts/consola.ttf",
  },
  times_new_roman: {
    name: "times",
    src: "public/fonts/times.ttf",
  },
  times_new_roman_italic: {
    name: "times_italic",
    src: "public/fonts/Times New Roman Italic.ttf",
  },
  楷体: {
    name: "simkai",
    src: "public/fonts/simkai.ttf",
  },
  楷体加粗: {
    name: "simkai_bold",
    src: "public/fonts/楷体加粗.ttf",
  },
  宋体: {
    name: "song",
    src: "public/fonts/华文宋体.ttf",
  },
  宋体加粗: {
    name: "song_bold",
    src: "public/fonts/宋体加粗.ttf",
  },
};

// preprocess
Object.keys(fonts).forEach((item) =>
  Font.register({ family: fonts[item]["name"], src: fonts[item]["src"] })
);

Font.registerHyphenationCallback((word) => {
  if (!/^[A-Za-z]+$/.test(word)) {
    return Array.from(word)
      .map((char) => [char, ""])
      .reduce((arr, current) => {
        arr.push(...current);
        return arr;
      }, []);
  } else {
    return word.length <= 10
      ? [word]
      : Array.from(word)
          .map((char) => [char, ""])
          .reduce((arr, current) => {
            arr.push(...current);
            return arr;
          }, []);
  }
});

// template
const Template = () => (
  <Document>
    <Cover>
      <Image src={pics["封面"]} allowDangerousPaths={true} />
      <Image
        src={pics["源古纪logo"]}
        style={{ position: "absolute", top: 100, left: 200, width: 200 }}
        allowDangerousPaths={true}
      />
      <Blank gap="100px" />
      <Title center>病原微生物宏基因组(mNGS)检测报告</Title>
      <Blank gap="30px" />
      <Box>
        <Table fontSize="10px" border="0px" cellpadding="9px 5px 0px">
          <Tr>
            <Td fontFamily="source_han_sans_bold">
              姓名：
              <CustomText fontFamily="source_han_sans">
                {infos["姓名"]}
              </CustomText>
            </Td>
            <Td fontFamily="source_han_sans_bold">
              性别：
              <CustomText fontFamily="source_han_sans">
                {infos["性别"]}
              </CustomText>
            </Td>
            <Td fontFamily="source_han_sans_bold">
              年龄：
              <CustomText fontFamily="source_han_sans">
                {infos["年龄"]}
              </CustomText>
            </Td>
          </Tr>
          <Tr>
            <Td fontFamily="source_han_sans_bold">
              送检单位：
              <CustomText fontFamily="source_han_sans">
                {infos["送检单位"]}
              </CustomText>
            </Td>
          </Tr>
          <Tr>
            <Td fontFamily="source_han_sans_bold">
              样本条形码：
              <CustomText fontFamily="source_han_sans">
                {infos["样本条形码"]}
              </CustomText>
            </Td>
            <Td fontFamily="source_han_sans_bold">
              报告时间：
              <CustomText fontFamily="source_han_sans">
                {infos["报告时间"]}
              </CustomText>
            </Td>
          </Tr>
        </Table>
      </Box>
    </Cover>
    <Layout>
      <Header fixed>
        <Image
          src={pics["呼研所logo"]}
          style={{ width: "27%" }}
          allowDangerousPaths={true}
        />
        <Image
          src={pics["源古纪logo"]}
          style={{ width: "23%" }}
          allowDangerousPaths={true}
        />
        <Image
          src={pics["惠善logo"]}
          style={{ width: "20%", height: "80%" }}
          allowDangerousPaths={true}
        />
      </Header>
      <Footer render={({ pageNumber }) => `- ${pageNumber - 1} -`} fixed />
      {/* <Image src={ pics["替换水印"] }
             style={{ position: "absolute", top: "200px", left: "50px", width: "100%" }}
             allowDangerousPaths={true}
             fixed
      /> */}
      <Section>
        <HeadlineBox>
          <Image src={pics["章节编号"]} allowDangerousPaths={true} />
          <Headline1>基本信息</Headline1>
        </HeadlineBox>
        <Blank gap="10px" />
        <Table font-size="10px" border="0.5px" cellpadding="5px 5px 0px">
          <Tr>
            <Td>样本条码号：{infos["样本条形码"]}</Td>
            <Td>样本号：{infos["样本号"]}</Td>
          </Tr>
          <Tr cellSizes={["20%", "80%"]}>
            <Td fontFamily="simkai_bold">检测项目</Td>
            <Td>病原微生物宏基因组测序（DNA+RNA）</Td>
          </Tr>
          <Tr>
            <Td render={() => " "} />
          </Tr>
          <Tr cellSizes={["20%", "80%"]}>
            <Td fontFamily="simkai_bold" center padding="35px 0px 0px">
              受检者信息
            </Td>
            <Tr direction="column" borderTopHidden borderBottomHidden>
              <Tr borderTopHidden>
                <Td fontFamily="simkai_bold" center>
                  姓名
                </Td>
                <Td center>{infos["姓名"]}</Td>
              </Tr>
              <Tr>
                <Td fontFamily="simkai_bold" center>
                  性别
                </Td>
                <Td center>{infos["性别"]}</Td>
              </Tr>
              <Tr>
                <Td fontFamily="simkai_bold" center>
                  年龄
                </Td>
                <Td center>{infos["年龄"]}</Td>
              </Tr>
              <Tr borderBottomHidden>
                <Td fontFamily="simkai_bold" center>
                  住院号/床号
                </Td>
                <Td center>{infos["住院号/床号"]}</Td>
              </Tr>
            </Tr>
          </Tr>
          <Tr>
            <Td render={() => " "} />
          </Tr>
          <Tr cellSizes={["20%", "80%"]}>
            <Td fontFamily="simkai_bold" center padding="25px 0px 0px">
              送检方信息
            </Td>
            <Tr direction="column" borderTopHidden borderBottomHidden>
              <Tr borderTopHidden>
                <Td fontFamily="simkai_bold" center>
                  医院名称
                </Td>
                <Td center>{infos["医院名称"]}</Td>
              </Tr>
              <Tr>
                <Td fontFamily="simkai_bold" center>
                  科室名称
                </Td>
                <Td center>{infos["科室名称"]}</Td>
              </Tr>
              <Tr borderBottomHidden>
                <Td fontFamily="simkai_bold" center>
                  送检医生
                </Td>
                <Td center>{infos["送检医生"]}</Td>
              </Tr>
            </Tr>
          </Tr>
          <Tr>
            <Td render={() => " "} />
          </Tr>
          <Tr cellSizes={["20%", "80%"]}>
            <Td fontFamily="simkai_bold" center padding="25px 0px 0px">
              样本信息
            </Td>
            <Tr direction="column" borderTopHidden borderBottomHidden>
              <Tr borderTopHidden>
                <Td fontFamily="simkai_bold" center>
                  样本类型
                </Td>
                <Td center>{infos["样本类型"]}</Td>
              </Tr>
              <Tr>
                <Td fontFamily="simkai_bold" center>
                  采样日期
                </Td>
                <Td center>{infos["采样日期"]}</Td>
              </Tr>
              <Tr borderBottomHidden>
                <Td fontFamily="simkai_bold" center>
                  收样日期
                </Td>
                <Td center>{infos["收样日期"]}</Td>
              </Tr>
            </Tr>
          </Tr>
          <Tr>
            <Td render={() => " "} />
          </Tr>
          <Tr cellSizes={["20%", "80%"]}>
            <Td fontFamily="simkai_bold" center padding="120px 0px 0px">
              临床信息
            </Td>
            <Tr direction="column" borderTopHidden borderBottomHidden>
              <Tr borderTopHidden>
                <Td fontFamily="simkai_bold" center>
                  主诉
                </Td>
                <Td center>{infos["主诉"]}</Td>
              </Tr>
              <Tr>
                <Td fontFamily="simkai_bold" center>
                  临床诊断
                </Td>
                <Td center>{infos["临床诊断"]}</Td>
              </Tr>
              <Tr>
                <Td fontFamily="simkai_bold" center>
                  临床高度关注病原
                </Td>
                <Td center>{infos["临床高度关注病原"]}</Td>
              </Tr>
              <Tr>
                <Td fontFamily="simkai_bold" center>
                  抗感染用药及时长
                </Td>
                <Td center>{infos["抗感染用药及时长"]}</Td>
              </Tr>
              <Tr>
                <Td fontFamily="simkai_bold" center>
                  白细胞计数(WBC)(10e9/L)
                </Td>
                <Td center>{infos["白细胞计数("]}</Td>
              </Tr>
              <Tr>
                <Td fontFamily="simkai_bold" center>
                  中性粒细胞计数(10e9/L)/比率
                </Td>
                <Td center>{infos["中性粒细胞计数"]}</Td>
              </Tr>
              <Tr>
                <Td fontFamily="simkai_bold" center>
                  淋巴细胞计数(10e9/L)/比率
                </Td>
                <Td center>{infos["淋巴细胞计数"]}</Td>
              </Tr>
              <Tr>
                <Td fontFamily="simkai_bold" center>
                  降钙素原(PCT) (ng/mL)
                </Td>
                <Td center>{infos["降钙素原"]}</Td>
              </Tr>
              <Tr>
                <Td fontFamily="simkai_bold" center>
                  C反应蛋白(CRP) (mg/L)
                </Td>
                <Td center>{infos["C反应蛋白"]}</Td>
              </Tr>
              <Tr>
                <Td fontFamily="simkai_bold" center>
                  培养结果
                </Td>
                <Td center>{infos["培养结果"]}</Td>
              </Tr>
              <Tr>
                <Td fontFamily="simkai_bold" center>
                  镜检结果
                </Td>
                <Td center>{infos["镜检结果"]}</Td>
              </Tr>
              <Tr borderBottomHidden>
                <Td fontFamily="simkai_bold" center>
                  其它
                </Td>
                <Td center>{infos["其它"]}</Td>
              </Tr>
            </Tr>
          </Tr>
        </Table>
      </Section>
      <Section break>
        <HeadlineBox>
          <Image src={pics["章节编号"]} allowDangerousPaths={true} />
          <Headline1>基本信息</Headline1>
        </HeadlineBox>
        <Blank gap="10px" />
        <Table
          font-size="10px"
          border="0.5px solid #5B9BD5"
          cellpadding="5px 5px 0px"
        >
          <Tr cellSizes={["17%", "83%"]}>
            <Td fontFamily="simkai_bold" center padding="40px 20px 0px">
              主要关注病原体
            </Td>
            <Tr direction="column" borderTopHidden borderBottomHidden>
              <Tr cellSizes={["30%", "70%"]} borderTopHidden>
                <Td fontFamily="simkai_bold" center>
                  细菌
                </Td>
                <Td fontFamily="simkai_bold" center>
                  {bacteria.length
                    ? bacteria
                        .map((i) => i.species.map((x) => x.c_name_s))
                        .flat()
                        .join("、")
                    : "无"}
                </Td>
              </Tr>
              <Tr cellSizes={["30%", "70%"]}>
                <Td fontFamily="simkai_bold" center>
                  真菌
                </Td>
                <Td fontFamily="simkai_bold" center>
                  {fungi.length
                    ? fungi
                        .map((i) => i.species.map((x) => x.c_name_s))
                        .flat()
                        .join("、")
                    : "无"}
                </Td>
              </Tr>
              <Tr cellSizes={["30%", "70%"]}>
                <Td fontFamily="simkai_bold" center>
                  病毒
                </Td>
                <Td fontFamily="simkai_bold" center>
                  {dna_virus.length + rna_virus.length
                    ? dna_virus
                        .map((i) => i.c_name_s)
                        .concat(rna_virus.map((i) => i.c_name_s))
                        .join("、")
                    : "无"}
                </Td>
              </Tr>
              <Tr cellSizes={["30%", "70%"]}>
                <Td fontFamily="simkai_bold" center>
                  寄生虫
                </Td>
                <Td fontFamily="simkai_bold" center>
                  {parasite.length
                    ? parasite.map((i) => i.c_name_s).join("、")
                    : "无"}
                </Td>
              </Tr>
              <Tr cellSizes={["30%", "70%"]}>
                <Td fontFamily="simkai_bold" center>
                  分枝杆菌复合菌群
                </Td>
                <Td fontFamily="simkai_bold" center>
                  {mycobacterium.length
                    ? mycobacterium.map((i) => i.c_name_s).join("、")
                    : "无"}
                </Td>
              </Tr>
              <Tr cellSizes={["30%", "70%"]}>
                <Td fontFamily="simkai_bold" center>
                  支原体/衣原体
                </Td>
                <Td fontFamily="simkai_bold" center>
                  {mycoplasma.length
                    ? mycoplasma.map((i) => i.c_name_s).join("、")
                    : "无"}
                </Td>
              </Tr>
              <Tr borderBottomHidden>
                <Td textIndent="20px">
                  {bacteria
                    .map((i) => i.species.map((x) => x.annotation))
                    .flat()
                    .concat(
                      fungi
                        .map((i) => i.species.map((x) => x.annotation))
                        .flat()
                    )
                    .concat(dna_virus.map((i) => i.annotation))
                    .concat(rna_virus.map((i) => i.annotation))
                    .concat(parasite.map((i) => i.annotation))
                    .concat(mycobacterium.map((i) => i.annotation))
                    .concat(mycoplasma.map((i) => i.annotation))
                    .filter((i) => i != "-")
                    .join("\n")}
                </Td>
              </Tr>
            </Tr>
          </Tr>
          <Tr cellSizes={["17%", "83%"]}>
            <Td fontFamily="simkai_bold" center padding="50px 20px 0px">
              次要关注病原体
            </Td>
            <Tr direction="column" borderTopHidden borderBottomHidden>
              <Tr cellSizes={["30%", "70%"]} borderTopHidden>
                <Td fontFamily="simkai_bold" center>
                  细菌
                </Td>
                <Td center>
                  {background_species["细菌"].map((i) => i.c_name_s).join("、")}
                </Td>
              </Tr>
              <Tr cellSizes={["30%", "70%"]}>
                <Td fontFamily="simkai_bold" center>
                  真菌
                </Td>
                <Td center>
                  {background_species["真菌"].map((i) => i.c_name_s).join("、")}
                </Td>
              </Tr>
              <Tr cellSizes={["30%", "70%"]}>
                <Td fontFamily="simkai_bold" center>
                  病毒
                </Td>
                <Td center>
                  {background_species["病毒"].map((i) => i.c_name_s).join("、")}
                </Td>
              </Tr>
              <Tr cellSizes={["30%", "70%"]}>
                <Td fontFamily="simkai_bold" center>
                  寄生虫
                </Td>
                <Td center>
                  {background_species["寄生虫"]
                    .map((i) => i.c_name_s)
                    .join("、")}
                </Td>
              </Tr>
              <Tr cellSizes={["30%", "70%"]}>
                <Td fontFamily="simkai_bold" center>
                  分枝杆菌复合菌群
                </Td>
                <Td center>
                  {background_species["分枝杆菌"]
                    .map((i) => i.c_name_s)
                    .join("、")}
                </Td>
              </Tr>
              <Tr cellSizes={["30%", "70%"]} borderBottomHidden>
                <Td fontFamily="simkai_bold" center>
                  支原体/衣原体
                </Td>
                <Td center>
                  {background_species["支衣原体"]
                    .map((i) => i.c_name_s)
                    .join("、")}
                </Td>
              </Tr>
            </Tr>
          </Tr>
          <Tr cellSizes={["17%", "83%"]}>
            <Td fontFamily="simkai_bold" center padding="20px 0 0">
              临床专家观点
            </Td>
            <Td textIndent="20px">{infos["临床专家观点"]}</Td>
          </Tr>
        </Table>
        <Blank gap="5px" />
        <CustomText fontSize="7px">
          <CustomText fontSize="7px" fontFamily="simkai_bold">
            注：
          </CustomText>
          指导意见出自广州呼吸健康研究院慢性气道疾病患者病原体二代测序结果解读实验室，该解读是由广州源古纪科技有限公司与广州呼吸健康研究院呼吸危重科二部（慢性气道疾病病区）联合提供服务。
        </CustomText>
        <Blank gap="20px" />
        <Sign>
          <SignItem>
            <CustomText fontSize="20px" fontFamily="song_bold">
              医生：
            </CustomText>
            <Image
              src={pics["医生签名"]}
              style={{ width: "70px", top: "-15px" }}
              allowDangerousPaths={true}
            />
          </SignItem>
          <SignItem>
            <CustomText fontSize="20px" fontFamily="song_bold">
              主任：
            </CustomText>
            <Image
              src={pics["主任签名"]}
              style={{ width: "50px", top: "-20px", right: "15px" }}
              allowDangerousPaths={true}
            />
          </SignItem>
        </Sign>
        <Image
          src={pics["呼研所logo"]}
          style={{ width: "120px", right: "-370px" }}
        />
        <Blank gap="10px" />
        <CustomText left="400px" fontFamily="simkai_bold" fontSize="7px">
          报告日期：{infos["报告时间"]}
        </CustomText>
      </Section>
      <Section break>
        <HeadlineBox>
          <Headline1 render={() => "mNGS 检测结果"} />
        </HeadlineBox>
        <Blank gap="10px" />
        <HeadlineBox noBorder>
          <Image src={pics["章节编号"]} allowDangerousPaths={true} />
          <Headline2>主要关注检测内容</Headline2>
        </HeadlineBox>
        <Blank gap="10px" />
        <Headline3 render={() => "1. 检出细菌列表"} />
        <Blank gap="15px" />
        <SpeciesTableStyle1 hasType data={bacteria} />
        <Blank gap="7px" />
        <CustomText fontSize="7px">
          <CustomText fontSize="7px" fontFamily="simkai_bold">
            注：
          </CustomText>
          <Superscript size="10px">G⁻</Superscript>
          代表革兰氏阴性菌，
          <Superscript size="10px">G⁺</Superscript>
          代表革兰氏阳性菌，Mapping序列数是指匹配到该病原体的序列数目，其多少与标本中病原体本身载量负荷、核酸提取量和人源序列比例有关，越高表示标本中检测到该病原体的可信度越高。属水平的Mapping序列数包含种水平Mapping序列数。
        </CustomText>
        <Blank gap="15px" />
        <Headline3 render={() => "2. 检出真菌列表"} />
        <Blank gap="15px" />
        <SpeciesTableStyle1 data={fungi} />
        <Blank gap="15px" />
        <Headline3 render={() => "3. 检出病毒列表"} />
        <Blank gap="15px" />
        <Headline4 render={() => "3.1 DNA病毒"} />
        <Blank gap="15px" />
        <SpeciesTableStyle2 data={dna_virus} />
        <Blank gap="15px" />
        <Headline4 render={() => "3.2 RNA病毒"} />
        <Blank gap="15px" />
        <SpeciesTableStyle2 data={rna_virus} />
        <Blank gap="15px" />
        <View break>
          <Headline3 render={() => "4. 检出寄生虫列表"} />
        </View>
        <Blank gap="15px" />
        <SpeciesTableStyle2 data={parasite} />
        <Blank gap="15px" />
        <Headline3 render={() => "5. 检出分枝杆菌复合群列表"} />
        <Blank gap="15px" />
        <SpeciesTableStyle2 data={mycobacterium} />
        <Blank gap="15px" />
        <Headline3 render={() => "6. 检出支原体或者衣原体列表"} />
        <Blank gap="15px" />
        <SpeciesTableStyle2 data={mycoplasma} />
        <Blank gap="15px" />
        <Headline3 render={() => "7. 检出耐药基因列表"} />
        <Blank gap="15px" />
        <GeneTable data={gene} />
        <Blank gap="7px" />
        <CustomText fontSize="7px">
          <CustomText fontSize="7px" fontFamily="simkai_bold">
            注：
          </CustomText>
          参考数据库CARD、ARDB和UNIPROT，比对一致率是衡量该序列与参考序列之间的相似度，可以用相同的碱基的占比来表示。多重耐药为未确定的耐药位点，仅做科研参考。
        </CustomText>
        <Blank gap="15px" />
        <HeadlineBox noBorder>
          <Image src={pics["章节编号"]} allowDangerousPaths={true} />
          <Headline2>次要关注检测结果附表</Headline2>
        </HeadlineBox>
        <Blank gap="15px" />
        <BackgroundTable data={Object.values(background_species).flat()} />
        <Blank gap="7px" />
        <CustomText fontSize="7px">
          <CustomText fontSize="7px" fontFamily="simkai_bold">
            注：
          </CustomText>
          次要关注检测结果列表中主要为口腔定植菌和条件致病菌，对于口腔定植菌除去菌群失调其他均置于次要结果，对于条件致病菌低于阳性阈值均置于次要结果。
        </CustomText>
        <Blank gap="15px" />
        <View break>
          <HeadlineBox>
            <Image src={pics["章节编号"]} allowDangerousPaths={true} />
            <Headline2>实验质控信息</Headline2>
          </HeadlineBox>
          <Blank gap="15px" />
          <Table
            fontSize="12px"
            border="0.5px solid black"
            cellpadding="10px 0 0"
          >
            <Tr>
              <Td center fontFamily="song_bold">
                测序总序列数
              </Td>
              <Td center fontFamily="song_bold">
                微生物序列数
              </Td>
              <Td center fontFamily="song_bold">
                内参检出率
              </Td>
              <Td center fontFamily="song_bold">
                阴阳性质控结果
              </Td>
            </Tr>
            <Tr>
              <Td center>{stats["总序列数"]}</Td>
              <Td center>{stats["微生物序列数"]}</Td>
              <Td center>{stats["内参检出率"]}</Td>
              <Td center>{stats["阴阳性质控"]}</Td>
            </Tr>
          </Table>
        </View>
        <Blank gap="7px" />
        <CustomText fontSize="7px" fontFamily="simkai_bold" height="2px">
          注：
        </CustomText>
        <CustomText fontSize="7px" height="2px">
          检出总序列数：经高通量测序方法检测得到的核酸序列总数；
        </CustomText>
        <CustomText fontSize="7px" height="2px">
          微生物检测序列数：样本中检测到的微生物核酸序列总数；
        </CustomText>
        <CustomText fontSize="7px" height="2px">
          内参检出率：设计一段非人源非微生物的序列质粒作为内参加入实验质控，内参序列的比对率；
        </CustomText>
        <CustomText fontSize="7px" height="2px">
          阴阳性质控：选用人hela细胞作为阴性质控，选用常见的微生物作为阳性质控监控每个run，检测结果符合即为合格；
        </CustomText>
        <CustomText fontSize="7px" height="2px">
          覆盖度：表示检测到的该微生物核酸序列覆盖到该微生物整个基因序列的比值；
        </CustomText>
        <CustomText fontSize="7px" height="2px">
          离散度：指检测到的微生物核酸序列在其基因组上分布的随机性，随机性越高，检测可信度越高；
        </CustomText>
        <CustomText fontSize="7px" height="2px">
          深度（depth）：指该区间上检出微生物核酸序列的次数。
        </CustomText>
        <Blank gap="15px" />
        <HeadlineBox>
          <Image src={pics["章节编号"]} allowDangerousPaths={true} />
          <Headline2>检出病原覆盖图及其参考文献</Headline2>
        </HeadlineBox>
        <Blank gap="200px" />
        <Sign width="630px" break>
          <SignItem>
            <CustomText fontSize="20px" fontFamily="song_bold">
              检测者：黄伟芬
            </CustomText>
          </SignItem>
          <SignItem>
            <CustomText fontSize="20px" fontFamily="song_bold">
              审核者：李鸿江
            </CustomText>
          </SignItem>
          <SignItem>
            <CustomText fontSize="20px" fontFamily="song_bold">
              审核日期：{infos["审核日期"]}
            </CustomText>
          </SignItem>
          <Image
            src={pics["惠善章"]}
            style={{
              positon: "absolute",
              top: "-40px",
              right: "200px",
              width: "130px",
            }}
            allowDangerousPaths={true}
          />
        </Sign>
      </Section>
      <Section break>
        <HeadlineBox>
          <Headline1 render={() => "附录："} />
        </HeadlineBox>
        <Blank gap="10px" />
        <HeadlineBox noBorder>
          <Image src={pics["章节编号"]} allowDangerousPaths={true} />
          <Headline2>项目介绍：</Headline2>
        </HeadlineBox>
        <Blank gap="7px" />
        <CustomText
          fontSize="12px"
          fontFamily="song"
          textIndent="24px"
          height="2px"
        >
          宏基因组测序是指对特定环境样品中的全部病原体基因组进行高通量测序，该方法能够快速、准确、高效地获得整个病原体群体的基因组信息。宏基因组测序不依赖于病原体的分离培养，可以得到环境中丰度较低甚至是痕量病原体的信息
          <Superscript size="10px">⁽¹⁻³⁾</Superscript>
          。近年来，宏基因组测序越来越多的应用于医学研究及临床诊断领域，如感染类型诊断、抗性基因的鉴定和传染病的防控等
          <Superscript size="10px">⁽⁴⁻⁹⁾</Superscript>。
        </CustomText>
        <Blank gap="7px" />
        <CustomText
          fontSize="12px"
          fontFamily="song"
          textIndent="24px"
          height="2px"
        >
          <Superscript />
          广州源古纪科技有限公司推出的病原微生物检测服务，将检测技术结合临床指导经验后进行综合分析，可检测范围包括31033种微生物，
          其中细菌14330 种（含 104 种分枝杆菌、 97
          种支原体/衣原体/螺旋体），真菌814种，病毒15720种（DNA病毒12894种，RNA病毒
          2826种），寄生虫169种，耐药基因4417种（31种抗生素）；针对呼吸感染，广州呼吸健康研究院有着丰富和领先的临床经验，为广州源古纪的检测提供临床指导，协助医生进行诊疗。
        </CustomText>
        <Blank gap="100px" />
        <Image
          src={pics["源古纪二维码"]}
          style={{ positon: "relative", left: "150px", width: "200px" }}
          allowDangerousPaths={true}
        />
      </Section>
      <Section break>
        <HeadlineBox>
          <Image src={pics["章节编号"]} allowDangerousPaths={true} />
          <Headline2>参考文献</Headline2>
        </HeadlineBox>
        <Blank gap="15px" />
        <CustomText fontSize="12px" fontFamily="times" height="2px">
          [1] Wilson M.R., et al. Clinical metagenomic next generation
          sequencing for diagnosis of infectious meningitis and encephalitis.
          [J] .N Engl J Med.2019.
        </CustomText>
        <CustomText fontSize="12px" fontFamily="times" height="2px">
          [2] Fan, S., et al. Metagenomic Next-generation Sequencing of
          Cerebrospinal Fluid for the Diagnosis of Central Nervous System
          Infections: A Multicentre Prospective Study. [J] bioRxiv,2019.
        </CustomText>
        <CustomText fontSize="12px" fontFamily="times" height="2px">
          [3] Blauwkamp, T. A., et al., Analytical and clinical validation of a
          microbial cell-free DNA sequencing test for infectious disease. [J]
          Nat Microbiol, 2019, 4(4), p.663.
        </CustomText>
        <CustomText fontSize="12px" fontFamily="times" height="2px">
          [4] Miller, S., et al. Laboratory validation of a clinical metagenomic
          sequencing assay for pathogen detection in cerebrospinal fluid.
          [J]Genome Res, 2019.
        </CustomText>
        <CustomText fontSize="12px" fontFamily="times" height="2px">
          [5] Chen H, Jiang W. Application of high-throughput sequencing in
          understanding human oral microbiome related with health and disease
          [J] . Front Microbiol, 2014, 5:508.
        </CustomText>
        <CustomText fontSize="12px" fontFamily="times" height="2px">
          [6] Guan,H.,et al. Detection of virus in CSF from the cases with
          meningoencephalitis by next-generation sequencing. [J] Journal of
          neurovirology, 2016.22(2), 240-245.
        </CustomText>
        <CustomText fontSize="12px" fontFamily="times" height="2px">
          [7] Lynch T,Petkau A,Knox N,et al. A primer on infectious disease
          bacterial genomics [J] . Clin Microbiol Rev, 2016, 29(4):881-913.
        </CustomText>
        <CustomText fontSize="12px" fontFamily="times" height="2px">
          [8] Lopez-Perez M, Mirete S. Discovery of novel antibiotic resistance
          genes through metagenomics [J] . Recent Adv DNA Gene Seq,2014,
          8(1):15-19.
        </CustomText>
        <CustomText fontSize="12px" fontFamily="times" height="2px">
          [9]Blum HE. The human microbiome [J] . Adv Med Sci,
          2017,62(2):414-420.
        </CustomText>
      </Section>
      <Section break>
        <HeadlineBox>
          <Image src={pics["章节编号"]} allowDangerousPaths={true} />
          <Headline2>检测须知</Headline2>
        </HeadlineBox>
        <Blank gap="15px" />
        <Headline3 render={() => "1. 送检流程："} />
        <Blank gap="15px" />
        <Image
          src={pics["送检流程"]}
          style={{ positon: "relative", width: "500px" }}
          allowDangerousPaths={true}
        />
        <Headline3 render={() => "2. 检测局限性："} />
        <Blank gap="7px" />
        <CustomText
          fontSize="13px"
          fontFamily="song"
          textIndent="24px"
          height="2px"
        >
          本检测非临床常规项目，目前主要用于提供指导意见辅助临床诊断，仅供临床参考。本检测报告不包括国家法定的甲类、乙类传染病。取样不当、标签脱落、或运输不当造成样本耗损或难以辨识造成无法检测，有可能需要重新采样。低于检测限的微生物不能保证可以检出。未检测出某种病原微生物，并不能排除受检者感染某种病原微生物的可能性。
        </CustomText>
        <CustomText
          fontSize="13px"
          fontFamily="song"
          textIndent="24px"
          height="2px"
        >
          临床研究表明，耐药基因与实际表型可能不完全一致，报告中的耐药基因检测结果仅供参考。患者使用抗感染药物可能会导致样本中微生物含量降低，影响检出率。若在采样前已使用抗感染药物，请如实提供用药史信息，否则可能导致检测失败。鉴于检测技术的局限以及患者的个体差异等因素，医院和检测机构在严格履行工作职责和操作规范的前提下，项目仍然存在无法检出或检测失败的可能。
        </CustomText>
        <Blank gap="15px" />
        <Headline3 render={() => "3. 免责声明："} />
        <Blank gap="7px" />
        <CustomText
          fontSize="13px"
          fontFamily="song"
          textIndent="24px"
          height="2px"
        >
          本检测结果仅对本次送检标本负责。因受检者知晓该结果可能带来的精神压力和心理负担，检测机构不承担连带责任。
        </CustomText>
      </Section>
    </Layout>
  </Document>
);

// components
const SpeciesTableStyle1 = (props) => {
  header = (
    <Tr direction="column" borderTopHidden borderBottomHidden>
      <Tr cellSizes={["43%", "57%"]} borderTopHidden>
        <Td center borderLeftHidden borderRightHidden fontFamily="song_bold">
          属
        </Td>
        <Td center borderLeftHidden borderRightHidden fontFamily="song_bold">
          种
        </Td>
      </Tr>
      <Tr
        cellSizes={["14%", "15%", "14%", "14%", "15%", "14%", "14%"]}
        borderTopHidden
        borderBottomHidden={props.hasType ? true : false}
      >
        <Td
          padding="13px 0 0"
          center
          borderLeftHidden
          borderRightHidden
          fontFamily="song_bold"
        >
          中文名
        </Td>
        <Td
          padding="13px 0 0"
          center
          borderLeftHidden
          borderRightHidden
          fontFamily="song_bold"
        >
          拉丁名
        </Td>
        {props.hasType ? (
          <Td center borderLeftHidden borderRightHidden fontFamily="song_bold">
            Mapping 序列数
          </Td>
        ) : (
          <Td center borderLeftHidden borderRightHidden fontFamily="song_bold">
            Mapping序列数
          </Td>
        )}
        <Td
          padding="13px 0 0"
          center
          borderLeftHidden
          borderRightHidden
          fontFamily="song_bold"
        >
          中文名
        </Td>
        <Td
          padding="13px 0 0"
          center
          borderLeftHidden
          borderRightHidden
          fontFamily="song_bold"
        >
          拉丁名
        </Td>
        {props.hasType ? (
          <Td center borderLeftHidden borderRightHidden fontFamily="song_bold">
            Mapping 序列数
          </Td>
        ) : (
          <Td center borderLeftHidden borderRightHidden fontFamily="song_bold">
            Mapping序列数
          </Td>
        )}
        <Td
          padding="13px 0 0"
          center
          borderLeftHidden
          borderRightHidden
          fontFamily="song_bold"
        >
          覆盖度
        </Td>
      </Tr>
    </Tr>
  );
  return props.hasType ? (
    <Table
      fontSize="12px"
      border="1px dashed #0070C0"
      cellpadding="5px 5px 0"
      borderLeftHidden
      borderRightHidden
      borderSize="5px"
      borderStyle="solid"
    >
      <Tr cellSizes={["10%", "90%"]} borderTopHidden>
        <Td
          padding="25px 0 0"
          center
          borderLeftHidden
          borderRightHidden
          fontFamily="song_bold"
        >
          类型
        </Td>
        {header}
      </Tr>
      {props.data.length == 0 ? (
        <Tr cellSizes={["10%", "90%"]} borderBottomHidden>
          <Td center borderLeftHidden borderRightHidden>
            未检出
          </Td>
          <Td center borderLeftHidden borderRightHidden></Td>
        </Tr>
      ) : (
        props.data.map((i, index) => {
          return (
            <Tr
              cellSizes={["10%", "12.6%", "13.5%", "12.6%", "51.3%"]}
              borderTopHidden
              borderBottomHidden={index == props.data.length - 1 ? true : false}
            >
              <Td center borderLeftHidden borderRightHidden>
                {i.type}
              </Td>
              <Td center borderLeftHidden borderRightHidden>
                {i.c_name_g}
              </Td>
              <Td
                fontFamily="times_italic"
                center
                borderLeftHidden
                borderRightHidden
              >
                {i.l_name_g}
              </Td>
              <Td center borderLeftHidden borderRightHidden>
                {i.g_reads}
              </Td>
              <Tr direction="column" borderTopHidden borderBottomHidden>
                {i.species.map((x) => (
                  <Tr
                    cellSizes={["24.5%", "26.5%", "24.5%", "24.5%"]}
                    borderTopHidden
                    borderBottomHidden
                  >
                    <Td center borderLeftHidden borderRightHidden>
                      {x.c_name_s}
                    </Td>
                    <Td
                      fontFamily="times_italic"
                      center
                      borderLeftHidden
                      borderRightHidden
                    >
                      {x.l_name_s}
                    </Td>
                    <Td center borderLeftHidden borderRightHidden>
                      {x.reads}
                    </Td>
                    <Td center borderLeftHidden borderRightHidden>
                      {x.coverage}
                    </Td>
                  </Tr>
                ))}
              </Tr>
            </Tr>
          );
        })
      )}
    </Table>
  ) : (
    <Table
      fontSize="12px"
      border="1px dashed #0070C0"
      cellpadding="5px 5px 0"
      borderLeftHidden
      borderRightHidden
      borderSize="5px"
      borderStyle="solid"
    >
      {header}
      {props.data.length == 0 ? (
        <Tr cellSizes={["14%", "86%"]} borderBottomHidden>
          <Td center borderLeftHidden borderRightHidden>
            未检出
          </Td>
          <Td center borderLeftHidden borderRightHidden></Td>
        </Tr>
      ) : (
        props.data.map((i, index) => {
          return (
            <Tr
              cellSizes={["14%", "15%", "14%", "57%"]}
              borderTopHidden
              borderBottomHidden={index == props.data.length - 1 ? true : false}
            >
              <Td center borderLeftHidden borderRightHidden>
                {i.c_name_g}
              </Td>
              <Td
                fontFamily="times_italic"
                center
                borderLeftHidden
                borderRightHidden
              >
                {i.l_name_g}
              </Td>
              <Td center borderLeftHidden borderRightHidden>
                {i.g_reads}
              </Td>
              <Tr direction="column" borderTopHidden borderBottomHidden>
                {i.species.map((x) => (
                  <Tr borderTopHidden borderBottomHidden>
                    <Td center borderLeftHidden borderRightHidden>
                      {x.c_name_s}
                    </Td>
                    <Td
                      fontFamily="times_italic"
                      center
                      borderLeftHidden
                      borderRightHidden
                    >
                      {x.l_name_s}
                    </Td>
                    <Td center borderLeftHidden borderRightHidden>
                      {x.reads}
                    </Td>
                    <Td center borderLeftHidden borderRightHidden>
                      {x.coverage}
                    </Td>
                  </Tr>
                ))}
              </Tr>
            </Tr>
          );
        })
      )}
    </Table>
  );
};

const SpeciesTableStyle2 = (props) => (
  <Table
    fontSize="12px"
    border="1px dashed #0070C0"
    cellpadding="10px 10px 5px"
    borderLeftHidden
    borderRightHidden
    borderSize="5px"
    borderStyle="solid"
  >
    <Tr cellSizes={["25%", "35%", "25%", "15%"]} borderTopHidden break>
      <Td center borderLeftHidden borderRightHidden fontFamily="song_bold">
        中文名
      </Td>
      <Td center borderLeftHidden borderRightHidden fontFamily="song_bold">
        拉丁名
      </Td>
      <Td center borderLeftHidden borderRightHidden fontFamily="song_bold">
        Mapping序列数
      </Td>
      <Td center borderLeftHidden borderRightHidden fontFamily="song_bold">
        覆盖度
      </Td>
    </Tr>
    {props.data.length == 0 ? (
      <Tr cellSizes={["25%", "75%"]} borderTopHidden borderBottomHidden>
        <Td center borderLeftHidden borderRightHidden>
          未检出
        </Td>
        <Td center borderLeftHidden borderRightHidden></Td>
      </Tr>
    ) : (
      props.data.map((i, index) => (
        <Tr
          cellSizes={["25%", "35%", "25%", "15%"]}
          borderTopHidden
          borderBottomHidden={index == props.data.length - 1 ? true : false}
        >
          <Td center borderLeftHidden borderRightHidden>
            {i.c_name_s}
          </Td>
          <Td
            fontFamily="times_italic"
            center
            borderLeftHidden
            borderRightHidden
          >
            {i.l_name_s}
          </Td>
          <Td center borderLeftHidden borderRightHidden>
            {i.reads}
          </Td>
          <Td center borderLeftHidden borderRightHidden>
            {i.coverage}
          </Td>
        </Tr>
      ))
    )}
  </Table>
);

const GeneTable = (props) => (
  <Table
    fontSize="12px"
    border="1px dashed #0070C0"
    cellpadding="10px 10px 5px"
    borderLeftHidden
    borderRightHidden
    borderSize="5px"
    borderStyle="solid"
  >
    <Tr borderTopHidden>
      <Td center borderLeftHidden borderRightHidden fontFamily="song_bold">
        基因
      </Td>
      <Td center borderLeftHidden borderRightHidden fontFamily="song_bold">
        抗生素
      </Td>
      <Td center borderLeftHidden borderRightHidden fontFamily="song_bold">
        比对一致率（%）
      </Td>
      <Td center borderLeftHidden borderRightHidden fontFamily="song_bold">
        常见菌属（种）
      </Td>
    </Tr>
    {props.data.length == 0 ? (
      <Tr cellSizes={["25%", "75%"]} borderTopHidden borderBottomHidden>
        <Td center borderLeftHidden borderRightHidden>
          未检出
        </Td>
        <Td center borderLeftHidden borderRightHidden></Td>
      </Tr>
    ) : (
      props.data.map((i, index) => (
        <Tr
          borderTopHidden
          borderBottomHidden={index == props.data.length - 1 ? true : false}
        >
          <Td center borderLeftHidden borderRightHidden>
            {i.基因}
          </Td>
          <Td center borderLeftHidden borderRightHidden>
            {i.抗生素}
          </Td>
          <Td center borderLeftHidden borderRightHidden>
            {i.比对一致率}
          </Td>
          <Td center borderLeftHidden borderRightHidden>
            {i.常见菌属}
          </Td>
        </Tr>
      ))
    )}
  </Table>
);

const BackgroundTable = (props) => (
  <Table
    fontSize="12px"
    border="1px dashed #0070C0"
    cellpadding="10px 10px 5px"
    borderLeftHidden
    borderRightHidden
    borderSize="5px"
    borderStyle="solid"
  >
    <Tr cellSizes={["15%", "15%", "15%", "27.5%", "27.5%"]} borderTopHidden>
      <Td
        center
        padding="18px 0 0"
        borderLeftHidden
        borderRightHidden
        fontFamily="song_bold"
      >
        中文名
      </Td>
      <Td
        center
        padding="18px 0 0"
        borderLeftHidden
        borderRightHidden
        fontFamily="song_bold"
      >
        拉丁名
      </Td>
      <Td center borderLeftHidden borderRightHidden fontFamily="song_bold">
        Mapping序列数
      </Td>
      <Td
        center
        padding="18px 0 0"
        borderLeftHidden
        borderRightHidden
        fontFamily="song_bold"
      >
        临床解释
      </Td>
      <Td
        center
        padding="18px 0 0"
        borderLeftHidden
        borderRightHidden
        fontFamily="song_bold"
      >
        参考文献
      </Td>
    </Tr>
    {props.data.length == 0 ? (
      <Tr cellSizes={["15%", "85%"]} borderTopHidden borderBottomHidden>
        <Td center borderLeftHidden borderRightHidden>
          未检出
        </Td>
        <Td center borderLeftHidden borderRightHidden></Td>
      </Tr>
    ) : (
      props.data.map((i, index) => (
        <Tr
          cellSizes={["15%", "15%", "15%", "27.5%", "27.5%"]}
          borderTopHidden
          borderBottomHidden={index == props.data.length - 1 ? true : false}
        >
          <Td center borderLeftHidden borderRightHidden>
            {i.c_name_s}
          </Td>
          <Td center borderLeftHidden borderRightHidden>
            {i.l_name_s}
          </Td>
          <Td center borderLeftHidden borderRightHidden>
            {i.reads}
          </Td>
          <Td borderLeftHidden borderRightHidden>
            {i.annotation}
          </Td>
          <Td borderLeftHidden borderRightHidden>
            {i.article}
          </Td>
        </Tr>
      ))
    )}
  </Table>
);

class Table extends React.Component {
  constructor() {
    super();
    this.renderChildren = this.renderChildren.bind(this);
  }

  renderChildren() {
    return React.Children.map(this.props.children, (child) => {
      return React.cloneElement(child, {
        border: this.props.border || "1px black solid",
        cellpadding: this.props.cellpadding || "0",
      });
    });
  }

  render() {
    return (
      <TableView
        wrap={this.props.wrap}
        fontSize={this.props.fontSize}
        fontFamily={this.props.fontFamily}
        border={this.props.border}
        borderTopHidden={this.props.borderTopHidden}
        borderBottomHidden={this.props.borderBottomHidden}
        borderLeftHidden={this.props.borderLeftHidden}
        borderRightHidden={this.props.borderRightHidden}
        borderSize={this.props.borderSize}
        borderStyle={this.props.borderStyle}
      >
        {this.renderChildren()}
      </TableView>
    );
  }
}

class Tr extends React.Component {
  constructor() {
    super();
    this.renderChildren = this.renderChildren.bind(this);
  }

  renderChildren() {
    return React.Children.map(this.props.children, (child, index) => {
      if (this.props.cellSizes) {
        return React.cloneElement(child, {
          wrap: false,
          width: this.props.cellSizes[index],
          border: this.props.border,
          cellpadding: this.props.cellpadding,
        });
      } else {
        count = React.Children.count(this.props.children);
        return React.cloneElement(child, {
          wrap: false,
          width: this.props.direction === "column" ? "100%" : 100 / count + "%",
          border: this.props.border,
          cellpadding: this.props.cellpadding,
        });
      }
    });
  }

  render() {
    return (
      <TrView
        width={this.props.width}
        border={this.props.border}
        borderTopHidden={this.props.borderTopHidden}
        borderBottomHidden={this.props.borderBottomHidden}
        direction={this.props.direction}
        center={this.props.center}
      >
        {this.renderChildren()}
      </TrView>
    );
  }
}

const TableView = styled.View`
  font-size: ${(props) => props.fontSize || "10px"};
  ${(props) => (props.fontFamily ? `font-family: ${props.fontFamily};` : "")}
  border: ${(props) => props.border || "1px"};
  ${(props) => (props.borderTopHidden ? "border-top-width: 0;" : "")}
  ${(props) => (props.borderBottomHidden ? "border-bottom-width: 0;" : "")}
  ${(props) => (props.borderLeftHidden ? "border-left-width: 0;" : "")}
  ${(props) => (props.borderRightHidden ? "border-right-width: 0;" : "")}
  ${(props) => (props.borderSize ? `border-style: ${props.borderSize};` : "")}
  ${(props) => (props.borderStyle ? `border-style: ${props.borderStyle};` : "")}
`;

const TrView = styled.View`
  position: relative;
  width: ${(props) => props.width || "100%"};
  display: flex;
  flex-direction: ${(props) => props.direction || "row"};
  // align-items: ${(props) => (props.center ? "center" : "stretch")};
  // justify-content: ${(props) => (props.center ? "center" : "flex-start")};
  border: ${(props) => props.border};
  border-left-width: 0;
  border-right-width: 0;
  ${(props) => (props.borderTopHidden ? "border-top-width: 0;" : "")}
  ${(props) => (props.borderBottomHidden ? "border-bottom-width: 0;" : "")}
`;

const Td = styled.Text`
  width: ${(props) => props.width};
  // display: flex;
  // flex-direction: row;
  // align-items: ${(props) => (props.center ? "center" : "stretch")};
  // justify-content: ${(props) => (props.center ? "center" : "flex-start")};
  border: ${(props) => props.border};
  border-top-width: 0;
  border-bottom-width: 0;
  ${(props) => (props.borderLeftHidden ? "border-left-width: 0;" : "")}
  ${(props) => (props.borderRightHidden ? "border-right-width: 0;" : "")}
  ${(props) => (props.borderColor ? `border-color: ${props.borderColor};` : "")}
  padding: ${(props) => props.padding || props.cellpadding};
  color: ${(props) => props.color};
  background-color: ${(props) => props.backgroundColor};
  font-family: ${(props) => props.fontFamily || "simkai"};
  font-size: ${(props) => props.fontSize || "10px"};
  text-align: ${(props) => (props.center ? "center" : "left")};
  text-indent: ${(props) => props.textIndent || 0};
`;

const Th = styled.Text`
  width: ${(props) => props.width};
  border: ${(props) => props.border};
  border-top-width: 0;
  border-bottom-width: 0;
  text-align: center;
  color: ${(props) => props.color};
  background-color: ${(props) => props.backgroundColor};
  padding: ${(props) => props.padding || props.cellpadding};
  font-family: ${(props) => props.fontFamily || "simkai"};
`;

// styles
const Cover = styled.Page`
  position: absolute;
  width: 100%;
  height: 100%;
  padding-top: 70px;
  padding-left: 50px;
  padding-right: 50px;
  font-size: 12px;
  font-family: simkai;
`;

const Blank = styled.View`
  height: ${(props) => props.gap};
`;

const Title = styled.Text`
  font-size: 20px;
  font-family: source_han_sans_bold;
  letter-spacing: 1px;
  text-align: ${(props) => (props.center ? "center" : "left")};
`;

const HeadlineBox = styled.View`
  position: relative;
  display: flex;
  flex-direction: row;
  border-bottom-width: ${(props) => (props.noBorder ? "0px" : "1px")};
  border-bottom-style: dashed;
`;

const Headline1 = styled.Text`
  font-size: 18px;
  font-family: song_bold;
  letter-spacing: 1px;
`;

const Headline2 = styled.Text`
  font-size: 14px;
  font-family: song_bold;
  letter-spacing: 1px;
`;

const Headline3 = styled.Text`
  font-size: 12px;
  font-family: song_bold;
  letter-spacing: 0.5px;
`;

const Headline4 = styled.Text`
  left: 20px;
  font-size: 10px;
  font-family: simkai_bold;
  letter-spacing: 0.5px;
`;

const CustomText = styled.Text`
  font-family: ${(props) => props.fontFamily || "simkai"}
  font-size: ${(props) => props.size || "10px"};
  text-indent: ${(props) => props.textIndent || 0};
  letter-spacing: ${(props) => props.letterSpacing || 0};
  left: ${(props) => props.left || 0};
  ${(props) => (props.height ? `line-height: ${props.height};` : "")}
`;

const Box = styled.View`
  left: 135px;
  width: 250px;
`;

const Layout = styled(Cover)`
  padding-bottom: 70px;
  padding-top: 40px;
`;

const Section = styled.View``;

const Header = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: 20px;
`;

const Footer = styled.Text`
  position: absolute;
  bottom: 30px;
  right: 300px;
  font-size: 10px;
`;

const Paragraph = styled.Text`
  font-family: ${(props) => props.fontFamily || "simkai"}
  font-size: ${(props) => props.size || "10px"};
  text-indent: ${(props) => props.textIndent || 0};
  letter-spacing: ${(props) => props.letterSpacing || 0};
`;

const Bold = styled.Text`
  font-family: simkai_bold;
`;

const Sign = styled.View`
  position: relative;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: ${(props) => props.width || "400px"};
`;

const SignItem = styled.View`
  position: relative;
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  width: 120px;
`;

const Superscript = styled.Text`
  font-family: consola;
  font-size: ${(props) => props.size || "10px"};
`;

// render
ReactPDF.render(<Template />);
