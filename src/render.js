import React from 'react'
import { transform } from 'buble'
import ReactPDF, {
    Page,
    View,
    Text,
    Image,
    StyleSheet,
    Font,
    Link,
    Canvas,
    Note
} from '@react-pdf/renderer';
import styled from '@react-pdf/styled-components'

const Document = 'DOCUMENT'

const primitives = { styled, Document, Page, Text, Link, Font, View, Note, Image, Canvas, StyleSheet}

var fs = require("fs")

const argv = process.argv
const code_file = argv[2]
const data_file = argv[3]
const path = argv[4]

var data = fs.readFileSync(data_file).toString()
var code = fs.readFileSync(code_file).toString()
var reg = /ReactPDF\.render\(.+\)/, str = reg.exec(code)[0]
code = code.replace(reg, str.substring(0, str.length - 1) + ", `" + path + "`)")

var element = "const report = " + data + "\n\n" + code
fs.writeFileSync(`code.js`, element)

try {
  const result = transform(element, {
    objectAssign: 'Object.assign',
    transforms: {
      dangerousForOf: true,
      dangerousTaggedTemplateString: true,
      moduleImport: false,
    }
  })

  // eslint-disable-next-line
  const res = new Function('React', 'ReactPDF', ...Object.keys(primitives), result.code)
  res(React, ReactPDF, ...Object.values(primitives))
  console.log('success')
} catch (e) {
  console.log(e)
}
