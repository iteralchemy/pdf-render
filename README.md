# 初始化
```
npm i && npm start
```
# 渲染示例
```
node src/render-compiled.js src/code.jsx src/data.json src/test.pdf
```
# 编译可执行程序(可选)
```
npm i -g pkg
pkg -t linux -o /path/to/Render .
```

